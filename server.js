'use strict';

const Path = require('path');
const Hapi = require('Hapi');
const Fb   = require('firebase');

const Server = Hapi.Server({
  port: 80,
  host: 'localhost'
});

const routes = require('./app.routes');
Server.route(routes);

Server.start(async (msg) => {
  await Server.register(require('vision'));

  if (!msg) throw msg;
});
