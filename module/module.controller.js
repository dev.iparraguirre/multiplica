const Md = require('./module.model');

getIdentify = (identify, callback) => {
  return Md.child(identify)
    .once('value', function (snap) {
      if (!snap.exists()) Obj.snapError = true;
    });
}

let Obj = module.exports = {
  snapError: false,
  setName: (req, pass) => {
    let insert = Md.push({
      name: req.payload.name,
      pais: 'peru',
      donacion: true
    }).key;

    return pass.response({ status: 'Ok', identify: insert });
  }
  , getName: (req, pass) => {
    let name;
    let promise = getIdentify(req.params.identify);

    return promise.then((snap) => {
      name = snap.val().name;
      return pass.response({ status: 'Ok', content: name });
    });
    //} else return pass.response({ status: 'Nodo no Existe' });
  }
  , sayHello: (req, pass) => {
    let name;
    let promise = getIdentify(req.params.identify);

    return promise.then((snap) => {
      name = snap.val().name;
      return pass.response({ status: 'Ok', content: 'Hola ' + name });
    });
    //} else return pass.response({ status: 'Nodo no Existe' });
  }
}