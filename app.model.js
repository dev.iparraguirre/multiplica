module.exports = {
  data : [
    {
      name: 'Pepe',
      dni: '70819853',
      pais: 'peru',
      placa: 'CXU613E',      
      donacion: true,
      esposas: ['Marily', 'Pepa'],
      edad: 32,
      sexo: 'M'
    },
    {
      name: 'Juan',
      dni: '36587495',   
      pais: 'peru',   
      placa: 'PLD687E',      
      donacion: true,
      esposas: ['Marily'],      
      edad: 19,
      sexo: 'M'
    },
    {
      name: 'Matias',
      dni: '85961528',  
      pais: 'peru',     
      placa: 'CXU613E',      
      donacion: false,
      esposas: [],      
      edad: 17,
      sexo: 'M'
    },
    {
      name: 'George',
      dni: '65857492',
      pais: 'peru',  
      placa: 'CXU613E',      
      donacion: true,
      esposas: ['Marta', 'Josefa'],      
      edad: 23,
      sexo: 'M'
    }    
  ]
}