const model = require('./app.model').data;

findInModel = (pass, type = false) => {
  type = typeof type == 'undefined' ? 0 : type;
  let returnObj = [];

  if (typeof pass == 'object') {
    pass.value = (pass.value.split(' ').join(''));
    if (!type) {
      let filterArr = pass.value.split(',');

      model.filter( (item) => {
        let i = Object.keys(item);
        let k = i.indexOf(filterArr[0]);
        let j = i.indexOf(filterArr[1]);

        if (k >= 0 && Object.entries(item)[k][1] == pass.value &&
          Object.entries(item)[j][1].length > 1)
          returnObj.push(item);
      });
    }
    else {
      let filterArr = pass.value.split('-');

      model.filter( (item) => {
        let i = Object.keys(item).indexOf('edad');
        if (Object.entries(item)[i][1] > filterArr[0] &&
          Object.entries(item)[i][1] < filterArr[1])
          returnObj.push(item.name);
      });
    }

    return returnObj;
  }
}

filterDriver = (type, val, response) => {
  let returnResponse = [];
  model.filter( (item) => {
    let i = Object.keys(item).indexOf(type);
    if (Object.entries(item)[i][1] == val) {
      let obj = {};
      response.forEach(e => {
        let keys = Object.keys(item).indexOf(e);
        obj[e] = Object.entries(item)[keys][1];
      });
      returnResponse.push(obj);
    }
  });

  return returnResponse;
}

var mod = module.exports = {
  filterMethod(req, pass) {
    let type = req.params.type;
    let parameter = req.params.parameter;
    let returnParameters = {};

    let compose = {};

    returnParameters['Mayores de Edad'] = mod.filterAndExclude(model);

    switch (type) {
      case 'donate':
        compose.param = 'donacion, esposas';
        compose.value = parameter;

        returnParameters['Primer Filtro'] = findInModel(compose);
        break;
      case 'exclude':
        compose.value = parameter;
        returnParameters['Segundo Filtro'] = findInModel(compose, true);
        break;
    }

    return pass.response(JSON.stringify(returnParameters));
  }
  , filterAndExclude(arr) {
    let returnItems = [];
    Array.prototype.forEach.call(arr, (item) => {
      if (item.edad >= 18)
        returnItems.push(item.name);
    });

    return JSON.stringify(returnItems);
  }
  , getDriver(req, pass) {
    let documentType = req.params.type.toLowerCase();
    let document = req.params.document;
    let returnResponse = [];

    if (['dni', 'placa'].includes(documentType)) {
      switch (documentType) {
        case 'dni':
          if (!(/^\d+$/).test(document) && document.length != 8)
            return pass.response({ msg: 'el dni no tiene los valores correctos' });

          return pass.response(JSON.stringify(filterDriver(documentType, document, ['pais', 'dni'])));
          break;

        case 'placa':
          return pass.response(JSON.stringify(filterDriver(documentType, document, ['pais', 'dni', 'sexo'])));
          break;
      }
    } else return pass.response({}).code(204);
  }
}