const controller = require('./app.controller');
const moduleController = require('./module/module.controller');
//const RoutesModule = require('./module/module.routes');

module.exports = [
  {
    path: '/',
    method: 'GET',
    handler: (r, h) => {
      return h.response({});
    }
  }
  , {
    path: '/persons/filter/{type}/{parameter}',
    method: 'GET',
    handler: controller.filterMethod
  }
  , {
    path: '/persons/getdriver/{type}/{document}',
    method: 'GET',
    handler: controller.getDriver
  }
  , {
    path: '/module/setname',
    method: 'POST',
    handler: moduleController.setName
  }
  , {
    path: '/module/getname/{identify}',
    method: 'GET',
    handler: moduleController.getName
  }
  , {
    path: '/module/sayhello/{identify}',
    method: 'GET',
    handler: moduleController.sayHello
  }
];